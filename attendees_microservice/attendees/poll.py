import json
import requests

from .models import ConferenceVO, AccountVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )


def get_accounts():
    url = "http://monolith:8000/api/accounts/"
    response = requests.get(url)
    content = json.loads(response.content)
    for account in content["accounts"]:
        AccountVO.objects.update_or_create(
            defaults={
                "email": account["email"],
                "first_name": account["first_name"],
                "last_name": account["last_name"],
                "is_active": account["is_active"],
                "updated": account["updated"],
            }
        )
